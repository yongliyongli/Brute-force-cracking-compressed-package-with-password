# 创建密码
class PsdIterator:
    # 单位字符集合
    letters = '012345678'  # 单元元素
    min_num = 0
    max_num = 0
    password = ''

    def __init__(self, min_num, max_num, digit=True, character=True):
        # 实例化对象时给出密码位数范围，一般4到10位
        if min_num < max_num:
            self.min_num = min_num
            self.max_num = max_num
        else:
            self.min_num = max_num
            self.max_num = min_num
        if digit or character:
            self.letters = ''
        if digit:
            self.letters += ''.join([str(i) for i in range(10)])
        if character:
            self.letters += ''.join([chr(i + ord('A')) for i in range(26)] + [chr(i + ord('a')) for i in range(26)])
        self.password = self.letters[0] * self.min_num

    # 迭代器访问定义
    def __iter__(self):
        return self

    def __next__(self):
        if len(self.password) > self.max_num:
            raise StopIteration
        else:
            result = self.password
            self.password = self.__newPsd(self.password)
            return result

    def __newPsd(self, oldValue):
        tmpList = list(oldValue)
        adder = 1   # 进位
        for index in range(len(oldValue) - 1, -1, -1):
            oldIndex = self.letters.index(tmpList[index])
            newIndex = (oldIndex + adder) % len(self.letters)
            adder = (oldIndex + adder)//len(self.letters)
            tmpList[index] = self.letters[newIndex]
        if adder != 0:
            tmpList.insert(0, self.letters[adder])
        return ''.join(tmpList)



